#include "dashboard.h"
#include <QFont>

Dashboard::Dashboard()
{
    step = new QGraphicsTextItem(this);
    step->setPos(100, 30);
    step->setPlainText("Step: " + QString::number(0));
    step->setFont(QFont("times", 20));
}

void Dashboard::setDashBoard(int t)
{
    step->setPlainText("Step: " + QString::number(t));
}

