#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include "board.h"
#include "dashboard.h"


class Game: public QGraphicsView{
    Q_OBJECT
public:
    // constructors
    Game(QWidget * parent=NULL);

    // public methods
    void start();
    int stateVec2num(QVector<int> state);
    void updateState(int subtask, int change);
    void updateStep();
    void updateMoveMax(QPair<int, int> lastMove);
    void updateMoveRational(QPair<int, int> lastMove);
    void updateMoveError(QPair<int, int> lastMove);
    void updateMoveGreedy(QPair<int, int> lastMove);
    void updateTrajectory();
    void updateTrajectory(int action);

    // public attributes
    QGraphicsScene* scene;
    Board* board;
    bool problemLoaded;

public slots:
    void savePattern();
    void reset();
    void load();
    void saveTrajectory();

private:
    int step;
    QVector<int> trajectory;
    QVector<int> state;
    Dashboard *dashboard;
    QVector<QVector<QStringList> > problem;
    int error_index;
    QVector<int> subtaskCount;
    QVector<int> stateCombinations;


};

#endif // GAME_H
