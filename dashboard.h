#ifndef DASHBOARD_H
#define DASHBOARD_H
#include <QGraphicsItem>


class Dashboard: public QGraphicsTextItem{
public:
    Dashboard();
    void setDashBoard(int t);
private:
    QGraphicsTextItem *step;
};

#endif // PANEL_H
