#ifndef BRICK_H
#define BRICK_H

#include <QGraphicsPolygonItem>
#include <QGraphicsSceneMouseEvent>

class Brick: public QGraphicsPolygonItem{
public:
    // constructors
    Brick(int scale=40, QGraphicsItem* parent=NULL);

    int status;
    int subtask;
    QPair<int, int> pos;
    QVector<int> owners;

    // getters
    int getScale();

    // public methods
    int flipStatus();

    // events
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);

private:
    int SCALE_BY;
};

#endif // BRICK_H
