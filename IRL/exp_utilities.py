import numpy as np
from xml.dom import minidom
import xml.etree.ElementTree as et

def xml_prettify(elem):
    """
    Return a pretty-printed XML string for the Element.
    """
    rough_string = et.tostring(elem, "utf-8")
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="    ", encoding="utf-8")

def generate_problem(tasks, filename):
    """
    Generate the problem from pattern files to a list where indices represent
    the subtask and each entry contains the components and owners of the subtask
    """

    components = set()
    for task in tasks:
        for (x,y), filled in np.ndenumerate(task):
            if filled == 1:
                components.add((x,y))

    # initilize ownership dictionary
    ownership = dict.fromkeys(components)

    # find owners
    for component in ownership.iterkeys():
        ownership[component] = set()
        for i, task in enumerate(tasks):
            if task[component] == 1:
                ownership[component].add(i)
        ownership[component] = tuple(ownership[component])

    # invert the mapping to get a partition of the components set
    partition = {}
    for component, owner in ownership.iteritems():
        partition[owner] = partition.get(owner, set())
        partition[owner].add(component)

    # generate the problem and write it into a text file

    sep = ','
    problem = []
    with open(filename, 'w') as f:
        counter = 0
        for i in partition.keys():  
            problem.append([tuple(partition[i]), i])
            f.write('E'+ str(counter) + "\n" + sep.join([str(x) for x in partition[i]]) + "\n" + "T" + str(i) + "\n")
            counter+=1

    return problem
