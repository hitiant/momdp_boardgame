import numpy as np


class BoardGame(object):

    def __init__(self, problem, error_prob=0.2, max_errors=2, discount=0.9, n_tasks=3):

        self.problem = problem
        self.n_tasks= n_tasks
        self.error_prob = error_prob
        self.max_errors = max_errors
        self.discount = discount
        self.error_index = len(self.problem)

        self.subtask_count = self.calc_subtask_count()
        self.state_combinations = self.calc_state_combinations()
        self.n_states = self.state_combinations[-1] * (self.max_errors + 1)
        self.n_actions = len(self.state_combinations)
        self.n_actions_momdp = 2*len(self.problem) + 1

        self.transition_matrix = np.zeros([self.n_states,
                                           self.n_actions,
                                           self.n_states])
        self.feature_matrix = np.zeros([self.n_states, 1 + self.error_index])

    def calc_subtask_count(self):
        return [len(subtask[0]) + 1 for subtask in self.problem]

    def calc_state_combinations(self):
        # calculate state combinations for each subtask count
        state_combinations = [1]
        subtask_count = self.subtask_count
        comb = 1
        for i, component_count in enumerate(subtask_count):
            comb *= component_count
            state_combinations.append(comb)
        return state_combinations

    def state_vec2num(self, state_vec):

        state_num = 0

        for i in range(len(self.state_combinations)):
            state_num += state_vec[i] * self.state_combinations[i]
        return state_num

    def state_num2vec(self, state_num):

        state_vec = []
        idx = -1

        while True:

            quotient = state_num / self.state_combinations[idx]
            remainder = state_num % self.state_combinations[idx]
            state_vec.insert(0, quotient)

            if remainder == 0:
                break
            else:
                state_num -= quotient * self.state_combinations[idx]
                idx -= 1

        rest_digits = [0] * (len(self.state_combinations) - len(state_vec))

        return rest_digits + state_vec
    
    def calc_feature_matrix(self):

        features = []
        max_subtask_count = [float(len(subtask[0])) for subtask in self.problem]
        max_subtask_count += [self.max_errors]
        from operator import div
        for state in range(self.n_states):
            state_vec = self.state_num2vec(state)
            f = map(div, state_vec, max_subtask_count)
            features.append(f)
        self.feature_matrix = np.array(features)
        

    def calc_transition_prob(self, next_state, action, curr_state):

        action_diff = 1
        error_prob = self.error_prob

        next_state_vec = self.state_num2vec(next_state)
        curr_state_vec = self.state_num2vec(curr_state)

        from operator import sub
        intermediate_state_vec = curr_state_vec[:]

        if action == self.error_index:

            action_diff = -1

            # if there is no error to correct, return 0 for all the states
            # except itself
            if curr_state_vec[self.error_index] == 0:

                if curr_state == next_state:
                    return 1
                else:
                    return 0

            # the instructor corrected an error
            intermediate_state_vec[action] += action_diff

            # the collaborator can make an error again
            next_state_error = intermediate_state_vec[:]
            next_state_error[self.error_index] += 1
            next_state_error = self.state_vec2num(next_state_error)

            if next_state == next_state_error:
                return error_prob

            # ...or go for the max. subtask
            max_subtask = self.find_max_subtask(intermediate_state_vec)

            if max_subtask == -1:
                # task completed, make no new action
                intermediate_state = self.state_vec2num(intermediate_state_vec)
                if next_state == intermediate_state:
                    # only self transition is allowed in this case
                    return 1 - error_prob
                else:
                    return 0

            next_state_max_subtask = intermediate_state_vec[:]
            next_state_max_subtask[max_subtask] += 1
            next_state_max_subtask = self.state_vec2num(next_state_max_subtask)

            if next_state == next_state_max_subtask:
                return 1 - error_prob
            else:
                return 0

        elif curr_state_vec[action] < self.subtask_count[action] - 2:

            intermediate_state_vec[action] += action_diff

            # the agent can make an error
            next_state_error = intermediate_state_vec[:]
            next_state_error[error_index] += 1
            next_state_error = self.state_vec2num(next_state_error)

            if next_state == next_state_error:
                return error_prob

            # ...or go for the same subtask
            next_state_same_subtask = intermediate_state_vec[:]
            next_state_same_subtask[action] += 1
            next_state_same_subtask = state_vec2num(next_state_same_subtask)

            if next_state == next_state_same_subtask:
                if next_state_error >= self.n_states:
                    # when it's not possible to make an error
                    return 1
                else:
                    # when it's still possible to make an error
                    return 1 - error_prob
            else:
                return 0

        else:

            intermediate_state_vec[action] += action_diff

            # if the subtask is already completed, return 0 for all states
            # except itself
            if curr_state_vec[action] >= self.subtask_count[action] - 1:

                if curr_state == next_state:
                    return 1
                else:
                    return 0

            # no more action possible in the current subtask

            # make an error
            next_state_error = intermediate_state_vec[:]
            next_state_error[self.error_index] += 1
            next_state_error = self.state_vec2num(next_state_error)

            if next_state == next_state_error:
                return error_prob

            # ...or go for the max. subtask
            max_subtask = self.find_max_subtask(intermediate_state_vec)

            if max_subtask == -1:
                # task is completed, make no new action
                intermediate_state = self.state_vec2num(intermediate_state_vec)

                if next_state == intermediate_state:
                    if next_state_error >= self.n_states:
                        # when it's not possible to make an error
                        return 1
                    else:
                        # when it's still possible to make an error
                        return 1 - error_prob
                else:
                    return 0

            next_state_max_subtask = intermediate_state_vec[:]
            next_state_max_subtask[max_subtask] += 1
            next_state_max_subtask = self.state_vec2num(next_state_max_subtask)

            if next_state == next_state_max_subtask:
                if next_state_error >= self.n_states:
                    # when it's not possible to make an error
                    return 1
                else:
                    # when it's still possible to make an error
                    return 1 - error_prob
            else:
                return 0

    def do_action(self, state, action, action_diff=1):
        state_vec = self.state_num2vec(state)
        state_vec[action] += action_diff
        return self.state_vec2num(state_vec)

    def find_max_subtask(self, state_vec):

        subtask_count = self.subtask_count
        n_subtask_owners = [len(s[1]) for s in self.problem]

        while True:

            if all(map(lambda x: x == -1, n_subtask_owners)):
                return -1

            max_value = max(n_subtask_owners)
            max_index = n_subtask_owners.index(max_value)

            if state_vec[max_index] < subtask_count[max_index] - 1:
                return max_index
            else:
                n_subtask_owners[max_index] = -1

    def calc_transition_matrix(self):

        for sas, _ in np.ndenumerate(self.transition_matrix):
            curr_state = sas[0]
            action = sas[1]
            next_state = sas[2]
            self.transition_matrix[sas] = self.calc_transition_prob(next_state,
                                                                    action,
                                                                    curr_state)
        self.check_transition_matrix()

    def check_transition_matrix(self):
        """
        sanity check of the transition matrix
        """

        assert np.isclose(self.transition_matrix.sum(axis=2),1).all(),\
        "transition probabilities do not sum to one."
        
