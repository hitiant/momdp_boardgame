#ifndef BOARD_H
#define BOARD_H

#include <QList>
#include "brick.h"

class Board
{
public:
    // constructors
    Board();

    // getters and setters
    QList<Brick*>* getBricks();
    QVector <QVector<Brick*> > brickMatrix;

    // public methods
    void placeBricks(int x, int y, int cols, int rows, int sizeBrick);
private:
    QList<Brick*> *bricks;
    void createBrickRow(int row, int x, int y, int numRows, int sizeBrick);
};

#endif // BOARD_H
