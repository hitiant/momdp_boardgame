#include "game.h"
#include "QPushButton"
#include "QRadioButton"
#include "QButtonGroup"
#include <QDebug>
#include <QFile>
#include <QTime>
#include <QTextStream>

Game::Game(QWidget *parent)
{
    problemLoaded = false;
    // set up the screen
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(1024, 768);

    // set up the scene
    scene = new QGraphicsScene();
    scene->setSceneRect(0, 0, 1024, 768);
    setScene(scene);

    // set up the setting button
    QPushButton *resetGame = new QPushButton();
    resetGame->setGeometry(QRect(700,100,200,50));
    resetGame->setText("Reset the Game");
    QGraphicsProxyWidget *proxyReset = scene->addWidget(resetGame);
    connect(resetGame, SIGNAL(clicked()), this, SLOT(reset()));

    QPushButton *loadProblem = new QPushButton();
    loadProblem->setGeometry(QRect(700,150,200,50));
    loadProblem->setText("Load Problem");
    QGraphicsProxyWidget *proxyLoad = scene->addWidget(loadProblem);
    connect(loadProblem, SIGNAL(clicked()), this, SLOT(load()));

    // set up the save pattern button
    QPushButton *savePattern = new QPushButton();
    savePattern->setGeometry(QRect(700,200,200,50));
    savePattern->setText("Save this Pattern");
    QGraphicsProxyWidget *proxyPattern = scene->addWidget(savePattern);
    connect(savePattern, SIGNAL(clicked()), this, SLOT(savePattern()));

    // set up the save trajectory button
    QPushButton *saveTrajectory = new QPushButton();
    saveTrajectory->setGeometry(QRect(700,250,200,50));
    saveTrajectory->setText("Save the trajectory");
    QGraphicsProxyWidget *proxyTrajectory = scene->addWidget(saveTrajectory);
    connect(saveTrajectory, SIGNAL(clicked()), this, SLOT(saveTrajectory()));

//    QGraphicsTextItem *prompt = new QGraphicsTextItem("What's the goal?");
//    prompt->setPos(700,290);
//    prompt->setFont(QFont("times", 20));
//    scene->addItem(prompt);
//    // set up the goal buttons
//    QButtonGroup *goals = new QButtonGroup();
//    for (size_t i=1;i<4;i++){
//        QRadioButton *goal = new QRadioButton();
//        goal->setGeometry(QRect(700,300+60*i,200,50));
//        goal->setText("Goal "+QString::number(i));
//        QGraphicsProxyWidget *proxy = scene->addWidget(goal);
//        goals->addButton(goal);
//    }
//    goals->exclusive();
    // set up the panel
    dashboard = new Dashboard();
    scene->addItem(dashboard);
}

void Game::start(){
    board = new Board();
    board->placeBricks(100, 100, 10, 10, 50);

}

int Game::stateVec2num(QVector<int> state)
{

    int stateNum = 0;
    for (int subtask = 0; subtask<problem.size() + 1 ; subtask++){
        stateNum += state[subtask]*stateCombinations[subtask];
    }

    return stateNum;
}

void Game::updateState(int subtask, int change){

    state[subtask] += change;

}

void Game::updateStep()
{
    step++;
    dashboard->setDashBoard(step);
}

void Game:: updateMoveMax(QPair<int, int> lastMove)
{

    // randomness
    float errorProb = 0.2;
    qsrand(QTime::currentTime().msec());
    int randNum = qrand() % 101;
    float p = randNum / 100.0;

    // make an error with probability = errorProb
    if (p <= errorProb){
        updateMoveError(lastMove);
        return;
    }

    // otherwise, make a move in the max. subtask
    // declare the move to be made
    QPair <int, int> thisMove;
    bool movePossible = false;
    QMultiMap<int, int> numOwners;

    // mapping from the number of the owners to its subtask
    for (int i = 0; i < problem.size(); i++){
        numOwners.insert(problem[i][1].size(), i);
    }

    // iterate over the subtasks, find possible moves
    QMapIterator<int, int> i(numOwners);
    i.toBack();
    int subtask;

    while (i.hasPrevious() && !movePossible) {

        i.previous();
        subtask = i.value();

        QStringList components = problem[subtask][0];
        foreach(QString c, components){
            c.remove(',');
            int x = c.at(0).digitValue();
            int y = c.at(1).digitValue();
            if (board->brickMatrix[x][y]->status == 0){
                movePossible = true;
                break;
            }
        }
    }

    if (!movePossible){
        // no move possible in any subtask
        // do nothing
        return;
    }

    // make a move that is the closest to the last move in the max. subtask
    QMultiMap<int, QPair<int, int> > dist2pos;
    QStringList components = problem[subtask][0];

    foreach(QString c, components){

        c.remove(',');
        int x = c.at(0).digitValue();
        int y = c.at(1).digitValue();

        if (board->brickMatrix[x][y]->status == 0){
            QPair<int, int> tempPos;
            tempPos.first = x;
            tempPos.second = y;
            int dist = abs(lastMove.first-x) + abs(lastMove.second-y);
            dist2pos.insert(dist, tempPos);
        }
    }

    thisMove = dist2pos.begin().value();
    qDebug() << "Move X:"  << thisMove.first << "Y:" << thisMove.second << "(Max)";
    int change = board->brickMatrix[thisMove.first][thisMove.second]->flipStatus();
    updateState(subtask, change);
}

void Game::updateMoveError(QPair<int, int> lastMove){

    QPair<int, int> thisMove;
    // find all possible errors
    QListIterator<Brick*> brickList((*board->getBricks()));
    QMultiMap<int, QPair<int, int> > dist2pos;

    while (brickList.hasNext()){
        Brick* brick = brickList.next();
        QPair<int,int> tempPos = brick->pos;
        if (brick->subtask == error_index &&
                brick->status == 0 &&
                tempPos != lastMove){

            int x = brick->pos.first;
            int y = brick->pos.second;
            int dist = abs(lastMove.first-x) + abs(lastMove.second-y);
            dist2pos.insert(dist, tempPos);
        }
    }

    // pick the closest error
    thisMove = dist2pos.begin().value();
    qDebug() << "Move X:"  << thisMove.first << "Y:" << thisMove.second << "(Error)";
    int change = board->brickMatrix[thisMove.first][thisMove.second]->flipStatus();
    updateState(error_index, change);

}

void Game::updateMoveGreedy(QPair<int, int> lastMove)
{
    QPair<int, int> thisMove;
    // find all possible moves
    QListIterator<Brick*> brickList((*board->getBricks()));
    QMultiMap<int, QPair<int, int> > dist2pos;

    while (brickList.hasNext()){
        Brick* brick = brickList.next();
        QPair<int,int> tempPos = brick->pos;
        if (brick->status == 0 && tempPos != lastMove){
            int x = brick->pos.first;
            int y = brick->pos.second;
            int dist = abs(lastMove.first-x) + abs(lastMove.second-y);
            dist2pos.insert(dist, tempPos);
        }
    }

    // pick the closest move
    thisMove = dist2pos.begin().value();
    int subtask = board->brickMatrix[thisMove.first][thisMove.second]->subtask;
    qDebug() << "Move X:"  << thisMove.first << "Y:" << thisMove.second << "(Greedy)";
    int change = board->brickMatrix[thisMove.first][thisMove.second]->flipStatus();
    updateState(subtask, change);

}

void Game::updateTrajectory(int action)
{
    trajectory.append(action);
    qDebug() << "Append action:" << trajectory;
}

void Game::updateTrajectory()
{
    trajectory.append(stateVec2num(state));
    qDebug() << "Append state:" << trajectory;
}

void Game::updateMoveRational(QPair<int, int> lastMove)
{
    QPair <int, int> thisMove;

    // if the last move is flipping an error
    // then it means that the instructor corrected an error
    // (the instructor never makes mistakes)

    // in this case, go for the max. subtask

    bool movePossible = false;
    int subtask = board->brickMatrix[lastMove.first][lastMove.second]->subtask;

    if (subtask == error_index){
        updateMoveMax(lastMove);
    }

    // otherwise, make a move that is the closest to the last move in the same subtask
    // and make errors randomly
    else{

        // make errors randomly

        float errorProb = 0.2;
        qsrand(QTime::currentTime().msec());
        int randNum = qrand() % 100;
        float p = randNum / 100.0;

        if (p<=errorProb){
            updateMoveError(lastMove);
            return;
        }

        else{

            QMultiMap<int, QPair<int, int> > dist2pos;
            QStringList components = problem[subtask][0];

            // construct ordered map from the distance to the component

            foreach(QString c, components){

                c.remove(',');
                int x = c.at(0).digitValue();
                int y = c.at(1).digitValue();

                if (board->brickMatrix[x][y]->status == 0){
                    movePossible = true;
                    QPair<int, int> tempPos;
                    tempPos.first = x;
                    tempPos.second = y;
                    int dist = abs(lastMove.first-x) + abs(lastMove.second-y);
                    dist2pos.insert(dist, tempPos);
                }
            }

            // if this subtask is not completed, go for the closest move in it
            if (movePossible){
                thisMove = dist2pos.begin().value();
                qDebug() << "Move X:"  << thisMove.first << "Y:" << thisMove.second << "(Rational)";
                int change = board->brickMatrix[thisMove.first][thisMove.second]->flipStatus();
                updateState(subtask, change);
            }

            // if there is no more possible move in the subtask
            else {
                updateMoveMax(lastMove);
            }

        }

    }
}

void Game::savePattern()
{
    // get the brick list
    QListIterator<Brick*> brickList((*board->getBricks()));

    // create a text file to save the pattern
    QString filename = "pattern.txt";
    QFile file(filename);

    if (file.open(QIODevice::ReadWrite)) {
        QTextStream stream(&file);
        while (brickList.hasNext()){
            stream << brickList.next()->status << " ";
        }
    }

}

void Game::reset()
{
    // get the brick list
    QListIterator<Brick*> brickList((*board->getBricks()));

    // clear every brick
    while (brickList.hasNext()){
        Brick* brick = brickList.next();

        QBrush brush;
        brush.setStyle(Qt::SolidPattern);
        brush.setColor(Qt::white);
        brick->setBrush(brush);

        brick->status = 0;
        brick->subtask = -1;
        brick->owners.clear();

    }

    // set step counter to zero, clear the state
    step = 0;
    dashboard->setDashBoard(step);
    viewport()->update();
    state.clear();
    trajectory.clear();
    problemLoaded = false;

}

void Game::load()
{
    // reset the game
    reset();
    // read the file

    QString filename ="problem.txt";

    QFile file(filename);
    if(!file.exists()){
        qDebug() << "File does not exist"<<filename;
    }else{
        qDebug() << filename<<"Loading...";
    }


    // some dirty parsing

    QString line;

    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream stream(&file);
        int currentClass = 0;

        while (!stream.atEnd()){

            line = stream.readLine();

            if (line.at(0) == QChar('E')) {
                line.remove(0, 1);
                currentClass = line.toInt();
                problem.resize(currentClass + 1);
            }

            else if (line.at(0) == QChar('T')){
                line.remove(0, 1);
                line.remove(QChar(' '));
                line.remove(QChar('('));
                line.remove(QChar(')'));
                QRegExp re(","); // match a comma
                QStringList list = line.split(re, QString::SkipEmptyParts);
                problem[currentClass].append(list);
            }
            else{
                // parse the line
                line.remove(QChar('\('));
                line.remove(QChar(' '));
                QRegExp re("(\\),)|(\\))");// match a bracket or a bracket and a comma
                QStringList list = line.split(re, QString::SkipEmptyParts);
                problem[currentClass].append(list);
            }
        }
        state.resize(currentClass + 2);
    }
    file.close();

    // update the ownership of the components

    // for errors
    error_index = problem.size();
    QListIterator<Brick*> brickList((*board->getBricks()));
    while (brickList.hasNext()){
        brickList.next()->subtask = error_index;
    }

    // for subtasks
    for (int i = 0; i < problem.size(); i++){
        QStringList components = problem[i][0];
        QStringList owners = problem[i][1];

        foreach(QString c, components){

            c.remove(',');
            int x = c.at(0).digitValue();
            int y = c.at(1).digitValue();

            foreach(QString owner, owners){
                board->brickMatrix[x][y]->owners.append(owner.toInt());
                board->brickMatrix[x][y]->subtask = i;
            }
        }
    }
    problemLoaded = true;

    // use the subtask count to calculate the number of state combinations
    // for state vector/index conversion

    subtaskCount.resize(problem.size());
    stateCombinations.append(1);

    for (int i = 0; i < problem.size(); i++){
        int maxCount = problem[i][0].size();
        subtaskCount[i] = maxCount + 1;
    }

    int comb = 1;

    for (int i = 0; i < problem.size(); i++){

        comb *= subtaskCount[i];
        stateCombinations.append(comb);
    }
    trajectory.append(stateVec2num(state));

    qDebug() << trajectory;
    //    qDebug() << stateCombinations;
}

void Game::saveTrajectory()
{
    if (problemLoaded){
        trajectory.append(error_index); // padding the last action as the error correction
        QVectorIterator<int> trajectoryIterator(trajectory);
        QString filename = "trajectory.txt";
        QFile file(filename);

        if (file.open(QIODevice::ReadWrite)) {
            QTextStream stream(&file);
            while (trajectoryIterator.hasNext()){
                stream << trajectoryIterator.next() << " ";
            }
        }
    }
}
