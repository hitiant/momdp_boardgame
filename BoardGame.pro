#-------------------------------------------------
#
# Project created by QtCreator 2016-04-07T14:54:59
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BoardGame
TEMPLATE = app


SOURCES += main.cpp \
    game.cpp \
    brick.cpp \
    board.cpp \
    dashboard.cpp

HEADERS  += \
    game.h \
    brick.h \
    board.h \
    dashboard.h

FORMS    +=
