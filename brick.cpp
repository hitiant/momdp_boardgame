#include "brick.h"
#include <QBrush>
#include "game.h"
#include <QDebug>

extern Game* game;

Brick::Brick(int scale, QGraphicsItem *parent){


    // set states as unfilled
    status = 0;
    subtask = -1;

    //draw
    QVector<QPointF> cornerPoints;
    cornerPoints << QPointF(0,0) << QPointF(1,0)
                 << QPointF(1,1) << QPointF(0,1);
    SCALE_BY = scale;

    for (size_t i = 0, n = cornerPoints.size(); i<n; i++){
        cornerPoints[i] *= SCALE_BY;
    }

    // create the brick
    QPolygonF brick(cornerPoints);
    setPolygon(brick);

    // events
    setAcceptHoverEvents(true);
}

int Brick::getScale()
{
    return SCALE_BY;
}

int Brick::flipStatus()
{
    // change the brick status
    int change = 0;
    if (status == 0){
        status = 1;
        QBrush brush;
        brush.setStyle(Qt::SolidPattern);
        brush.setColor(Qt::black);
        setBrush(brush);
        change = 1;
    }
    else if (status == 1){
        status = 0;
        QBrush brush;
        brush.setStyle(Qt::SolidPattern);
        brush.setColor(Qt::white);
        setBrush(brush);
        change = -1;
    }

    return change;
}


void Brick::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    // Debug information
    qDebug() << "X:" << pos.first << "Y:" << pos.second;

    int change = flipStatus();
    game->updateStep();

    if (game->problemLoaded){
        game->updateTrajectory(subtask);
        game->updateState(subtask, change);
        game->updateMoveRational(pos);
        game->updateTrajectory();
    }

}

void Brick::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    // change color to gray if it's white (unfilled)
    if (status==0){
        QBrush brush;
        brush.setStyle(Qt::SolidPattern);
        brush.setColor(Qt::gray);
        setBrush(brush);
    }
}

void Brick::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    // change color to gray if it's white (unfilled)
    if (status==0){
        QBrush brush;
        brush.setStyle(Qt::SolidPattern);
        brush.setColor(Qt::white);
        setBrush(brush);
    }
}
