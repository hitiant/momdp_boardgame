#include "board.h"
#include "game.h"
#include <QDebug>

extern Game* game;

Board::Board(){

}

QList<Brick *> * Board::getBricks(){
    return bricks;
}

void Board::placeBricks(int x, int y, int cols, int rows, int sizeBrick){

    // initialize containers
    bricks = new QList<Brick*>;
    brickMatrix.resize(rows);

    for(int i = 0; i < rows; i++) {
      brickMatrix[i].resize(cols);
    }

    // place the bricks on the board
    for (size_t i = 0, n = rows; i < n; i++){
        createBrickRow(i, x, y + sizeBrick*i, cols, sizeBrick);
    }
}

void Board::createBrickRow(int row, int x, int y, int numCols, int sizeBrick){
    // create a column of bricks at (x,y) with numRows rows

    for (size_t j = 0, n = numCols; j < n; j++){

        // create the brick
        Brick* brick = new Brick(sizeBrick);
        // set the position in the scene
        brick->setPos(x + brick->getScale()*j, y);
        // set the position in the matrix
        brick->pos.first = row;
        brick->pos.second = j;
        // add the brick to the scene
        game->scene->addItem(brick);

        // save the pointer in the containers
        brickMatrix[row][j] = brick;

        bricks->append(brick);
    }
}

